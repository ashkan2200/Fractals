package com.kaarako;
import java.awt.*;

/**
 * Created by alidorostkar on 06/06/16.
 */

/*
 * This interface is to be used with fractals
 * Each fractal needs to be able to return an
 * State object which is used internally to
 * Draw the fractal.
 * The method "draw" is used to actually output the fractal.
 */
public interface fractal {
    void draw(Graphics g, int width, int height);
    void setState(States st);
    States getState();
}
