package com.kaarako;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by alidorostkar on 06/06/16.
 */
public class Dragon implements fractal {
    private States _state;

    // Give birth to the Dragon,
    // Outputs:
    //      ArrayList<Integer> directions : The direction of each line in the dragon
    //                  This way one can draw all the lines by looking at the starting
    //                  position and following.
    // Input:
    //      iteration: Number of iterations to use for the dragon
    // Extra Info, Directions number: 0 = east, 1 = north, 2 = west, 3 = south.
    private ArrayList<Integer> dragonBirth(int iteration){
        ArrayList<Integer> directions = new ArrayList<>();
        ArrayList<Integer> temp       = new ArrayList<>();

        // Go east from starting point
        directions.add(0);

        for( int i = 0; i < iteration; i++){
            // loop backwards, turn 90 degrees and insert in the temp list
            for( int j = directions.size() - 1; j >= 0; j--)
                temp.add( (directions.get(j) + 1) % 4);

            // Append the original directions at the end.
            temp.addAll(directions);

            // Empty the direction list and put the temp list
            directions.clear();
            directions.addAll(temp);
            temp.clear();
        }

        return directions;
    }

    // This method is used to see how the dragon expands to the sides
    // of the starting point. It is used to correctly place the starting point on the screen.
    private int[] divergance(ArrayList<Integer> directions){
        int hor_div = 0;
        int ver_div = 0;
        int[] counts = new int[4];
        Arrays.fill(counts, 0);

        // It is the same concept as parenthesis counting.
        // For each open parenthesis add one, for each closing, remove one.
        for(Integer s : directions){
            switch (s) {
                case 0:
                    hor_div++;
                    break;
                case 1:
                    ver_div--;
                    break;
                case 2:
                    hor_div--;
                    break;
                case 3:
                    ver_div++;
                    break;
                default:
                    break;
            }
            // If the value is positive we are east or north of the starting point.
            // If the value is negative we are west or south of the starting point.
            if(hor_div >= 0 && counts[0] < hor_div)
                counts[0] = hor_div;
            if(hor_div <= 0 && counts[2] < -hor_div)
                counts[2] = -hor_div;
            if(ver_div <= 0 && counts[1] < -ver_div)
                counts[1] = -ver_div;
            if(ver_div >= 0 && counts[3] < ver_div)
                counts[3] = ver_div;
        }

        return counts;
    }

    public Dragon(){
        _state = States.getInstance();
        _state.setItr(10); // use 10 iterations, the size of the dragon is 2^it
        _state.setIterationRate(1); // Change iteration by one
        _state.setIterationChangeType(OPERATION.ADD); // Add iterations instead of multiplication
    }

    @Override
    public void draw(Graphics g, int width, int height) {
        ArrayList<Integer> directions = dragonBirth(_state.getIteration());

        // Compute the line size based on the number of lines.
        int[] counts = divergance(directions);

        int lineL;

        lineL = width/(counts[0] + counts[2]);
        if(lineL > height/(counts[1] + counts[3]))
            lineL = height/(counts[1] + counts[3]);

        lineL = lineL < 1 ? 1 : lineL;

        g.setColor(Color.red);

        // Get the starting point location
        int endX = lineL*counts[2]+10;
        int endY = lineL*counts[1]+10;
        int startX, startY;
        for(int d : directions){
            startX = endX;
            startY = endY;

            // Depending on the direction, draw the line.
            switch (d) {
                case 0:
                    endX = startX + lineL;
                    break;
                case 1:
                    endY = startY - lineL;
                    break;
                case 2:
                    endX = startX - lineL;
                    break;
                case 3:
                    endY = startY + lineL;
                    break;
            }
            g.drawLine(startX, startY, endX, endY);
        }
    }

    @Override
    public void setState(States st) {
        _state = st;
    }

    @Override
    public States getState() {
        return _state;
    }
}
