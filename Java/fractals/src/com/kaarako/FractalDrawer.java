package com.kaarako;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by alidorostkar on 06/06/16.
 */
public class FractalDrawer extends JPanel implements KeyListener{
    private ArrayList<fractal> fractalList = new ArrayList<>();
    private int topList = 0;
    private boolean print_help = true;
    private static HashMap<String, String> help_dic = new HashMap<String,String>(){
        {
            put("+" , "Add iterations");
            put("-" , "Reduce iterations");
            put("r" , "Reset view");
            put("f" , "Next fractal");
            put("arrow keys" , "Move around");
            put("z" , "Zoom in");
            put("x" , "Zoom out");
            put("q" , "Quit application");
            put("h" , "Toggle help");
        }
    };

    public void changeFractal(){
        topList = (topList+1)%fractalList.size();
    }

    public void registerFractal(fractal f){
        fractalList.add(f);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        setBackground(Color.black);

        fractal f = fractalList.get(topList);
        f.draw(g, getWidth(), getHeight());

        if(print_help) {
            print_help(g);
        }
    }

    private void print_help(Graphics g) {
        int font_size = 9;
        int hor_p = 10;
        int ver_spacing = 0;

        int temp = 0;
        for(String key : help_dic.keySet() ) {
            temp = g.getFontMetrics().stringWidth(key) + g.getFontMetrics().stringWidth(help_dic.get(key)) + 3;
            if (temp > ver_spacing)
                ver_spacing = temp;
        }

        g.setFont(new Font("Arial", Font.PLAIN, font_size));
        g.setColor(Color.white);
        String shift_st = "Holding <shift> performs the actions faster";
        g.drawString(shift_st, getWidth() - g.getFontMetrics().stringWidth(shift_st) - 10, hor_p);

        hor_p += font_size + 3;

        shift_st = String.format("%s: %d", "Number of iterations", fractalList.get(topList).getState().getIteration());
        g.drawString(shift_st, getWidth() - g.getFontMetrics().stringWidth(shift_st) - 10, hor_p);

        hor_p += font_size + 3;
        for(String key : help_dic.keySet() ) {
            g.drawString(String.format("%s : %s", key, help_dic.get(key)), getWidth() - ver_spacing, hor_p);
            hor_p +=  font_size;
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
        int shift_down = e.isShiftDown() ? 1 : 0;
        switch (e.getKeyCode()){
            case KeyEvent.VK_PLUS:
            case KeyEvent.VK_EQUALS:
                fractalList.get(topList).getState().addItr();
                break;
            case KeyEvent.VK_MINUS:
                fractalList.get(topList).getState().reduceItr();
                break;
            case KeyEvent.VK_R:
                fractalList.get(topList).getState().reset();
                break;
            case KeyEvent.VK_F:
                topList = (topList + 1)%fractalList.size();
                break;
            case KeyEvent.VK_RIGHT:
                fractalList.get(topList).getState().moveRight(shift_down*3. + 1.);
                break;
            case KeyEvent.VK_LEFT:
                fractalList.get(topList).getState().moveLeft(shift_down*3. + 1.);
                break;
            case KeyEvent.VK_UP:
                fractalList.get(topList).getState().moveUp(shift_down*3. + 1.);
                break;
            case KeyEvent.VK_DOWN:
                fractalList.get(topList).getState().moveDown(shift_down*3. + 1.);
                break;
            case KeyEvent.VK_Z:
                fractalList.get(topList).getState().zoomIn(shift_down*3. + 1.);
                break;
            case KeyEvent.VK_X:
                fractalList.get(topList).getState().zoomOut(shift_down*3. + 1.);
                break;
            case KeyEvent.VK_Q:
                System.exit(0);
                break;
            case KeyEvent.VK_H:
                print_help = !print_help;
                break;
            default:
                break;
        }
        repaint();
    }

    @Override
    public void keyTyped(KeyEvent e) {    }
    @Override
    public void keyReleased(KeyEvent e) {}
}
