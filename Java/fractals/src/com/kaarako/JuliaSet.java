package com.kaarako;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by alidorostkar on 06/06/16.
 */
public class JuliaSet implements fractal {
    private States _state = States.getInstance();

    private Image makeImage(int width, int height){
        BufferedImage bfi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

        int i;
        double cRe, cIm;                     //real and imaginary part of the constant c, determinate shape of the Julia Set
        double newRe, newIm, oldRe, oldIm;   //real and imaginary parts of new and old
        Color c; //the RGB color value for the pixel

        double zoom = _state.getZoom();
        double moveX = _state.getMoveX();
        double moveY = _state.getMoveY();
        double maxIterations = _state.getIteration();

        //pick some values for the constant c, this determines the shape of the Julia Set
        cRe = -0.7;
        cIm = 0.27015;
        for(int x = 0; x < width; x++){
            for(int y = 0; y < height; y++){
                newRe = 1.5 * (x - width / 2) / (0.5 * zoom * width) + moveX;
                newIm = (y - height / 2) / (0.5 * zoom * height) + moveY;
                //start the iteration process
                for(i = 0; i < maxIterations; i++){
                    //remember value of previous iteration
                    oldRe = newRe;
                    oldIm = newIm;
                    //the actual iteration, the real and imaginary part are calculated
                    newRe = oldRe * oldRe - oldIm * oldIm + cRe;
                    newIm = 2 * oldRe * oldIm + cIm;
                    //if the point is outside the circle with radius 2: stop
                    if((newRe * newRe + newIm * newIm) > 4) break;
                }
                //use color model conversion to get rainbow palette, make brightness black if maxIterations reached
                float h = (i % 256) / 255f;
                float b = (i < maxIterations) ? 1 : 0;
                c = Color.getHSBColor(h, 1, b);
                //draw the pixel
                bfi.setRGB(x, y, c.getRGB());
            }
        }

        return bfi;
    }
    @Override
    public void draw(Graphics g, int width, int height) {
        Image bfi = makeImage(width, height);
        Graphics2D g2 = (Graphics2D)g;
        g2.drawImage(bfi, null, null);
    }

    @Override
    public void setState(States st) {
        _state = st;
    }

    @Override
    public States getState() {
        return _state;
    }
}
