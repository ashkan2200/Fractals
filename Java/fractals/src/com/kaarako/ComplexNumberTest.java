package com.kaarako;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by alidorostkar on 10/06/16.
 */
public class ComplexNumberTest {
    @Test
    public void norm() throws Exception {
        ComplexNumber c = new ComplexNumber(2,0);
        assertEquals(2, c.norm(), 1e-14);
    }

    @Test
    public void polarDegree() throws Exception {
        ComplexNumber c = new ComplexNumber(2,2);
        assertEquals(Math.PI/4., c.phase(), 1e-12);
    }

    @Test
    public void pow() throws Exception {
        ComplexNumber c = ComplexNumber.fromPolar(2, Math.PI/4);
        c = c.pow(2);
        assertEquals(0 , c.getReal(), 1e-12);
        assertEquals(4., c.getImag(), 1e-12);
    }

    @Test
    public void add() throws Exception {
        ComplexNumber c1 = new ComplexNumber(1, 2);
        ComplexNumber c2 = new ComplexNumber(3, 4);
        c2 = c2.add(c1);
        assertEquals(4. , c2.getReal(), 1e-12);
        assertEquals(6., c2.getImag(), 1e-12);
    }

    @Test
    public void add1() throws Exception {
        ComplexNumber c1 = new ComplexNumber(1, 2);
        c1 = c1.add(0.5);
        assertEquals(1.5 , c1.getReal(), 1e-12);
        assertEquals(2.  , c1.getImag(), 1e-12);
    }

    @Test
    public void subtract() throws Exception {
        ComplexNumber c1 = new ComplexNumber(1, 2);
        ComplexNumber c2 = new ComplexNumber(3, 4);
        c2 = c2.subtract(c1);
        assertEquals(2. , c2.getReal(), 1e-12);
        assertEquals(2., c2.getImag(), 1e-12);
    }

    @Test
    public void subtract1() throws Exception {
        ComplexNumber c1 = new ComplexNumber(1, 2);
        c1 = c1.subtract(0.5);
        assertEquals(.5 , c1.getReal(), 1e-12);
        assertEquals(2.  , c1.getImag(), 1e-12);
    }

    @Test
    public void mult() throws Exception {
        ComplexNumber c1 = new ComplexNumber(1, 2);
        ComplexNumber c2 = new ComplexNumber(3, 4);
        c2 = c2.mult(c1);
        assertEquals(-5. , c2.getReal(), 1e-12);
        assertEquals(10., c2.getImag(), 1e-12);
    }

    @Test
    public void mult1() throws Exception {
        ComplexNumber c1 = new ComplexNumber(1, 2);
        c1 = c1.mult(0.5);
        assertEquals(.5 , c1.getReal(), 1e-12);
        assertEquals(1.  , c1.getImag(), 1e-12);
    }

    @Test
    public void conj() throws Exception {
        ComplexNumber c1 = new ComplexNumber(1, 2);
        ComplexNumber c2 = c1.conj();
        assertEquals(c2.getReal() , c1.getReal(), 1e-12);
        assertEquals(-c2.getImag()  , c1.getImag(), 1e-12);
    }

    @Test
    public void fromPolar() throws Exception {
        ComplexNumber c = ComplexNumber.fromPolar(2, Math.PI/2.);
        assertEquals(0, c.getReal(), 1e-12);
        assertEquals(2, c.getImag(), 1e-12);
    }

}