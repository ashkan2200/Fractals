package com.kaarako;

/**
 * Created by alidorostkar on 06/06/16.
 */
enum OPERATION{
    ADD,
    MULTIPLY
}

public class States {
    private double      moveUnitValue;
    private double      zoomUnitValue;
    private double      frameTime;
    private double      zoom;
    private double      moveX;
    private double      moveY;
    private int         maxIterations;
    private int         _maxIterations = 300;
    private int         iterationRate;
    private OPERATION   itType;

    private States(){
        reset();
        itType = OPERATION.MULTIPLY;
        iterationRate = 2;
    }

    public static States getInstance(){
        return new States();
    }

    public void reset(){
        moveUnitValue = 0.001;
        zoomUnitValue = 1.001;
        frameTime = 64;
        zoom = 1;
        moveX = 0;
        moveY = 0;
        maxIterations = _maxIterations;
    }

    public void zoomIn(double ratio){
        zoom *= ratio*Math.pow(zoomUnitValue, frameTime);
    }

    public void zoomOut(double ratio){
        zoom /= ratio*Math.pow(zoomUnitValue, frameTime);
    }

    public void moveUp(double ratio){
        moveY -= ratio* moveUnitValue * frameTime / zoom;
    }

    public void moveDown(double ratio){
        moveY += ratio* moveUnitValue * frameTime / zoom;
    }

    public void moveLeft(double ratio){
        moveX -= ratio* moveUnitValue * frameTime / zoom;
    }

    public void moveRight(double ratio){
        moveX += ratio * moveUnitValue * frameTime / zoom;
    }

    public void addItr(){
        switch (itType) {
            case ADD:
                maxIterations += iterationRate;
                break;
            case MULTIPLY:
                maxIterations *= iterationRate;
                break;
        }
    }

    public void reduceItr(){
        if (maxIterations > iterationRate){
            switch (itType) {
                case ADD:
                    maxIterations -= iterationRate;
                    break;
                case MULTIPLY:
                    maxIterations /= iterationRate;
                    break;
            }
        }
    }

    public void setItr(int it){
        maxIterations = it;
        _maxIterations = it;
    }

    public void setIterationRate(int r){
        iterationRate = r;
    }

    public double getZoom(){
        return zoom;
    }

    public double getMoveX(){
        return moveX;
    }

    public double getMoveY(){
        return moveY;
    }

    public int getIteration(){
        return maxIterations;
    }

    public void setIterationChangeType(OPERATION op){
        itType = op;
    }
}
