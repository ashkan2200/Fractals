package com.kaarako;

/**
 * Created by alidorostkar on 10/06/16.
 */
public class ComplexNumber {
    final private double real;
    final private double imag;

    public double getReal(){
        return real;
    }

    public double getImag(){
        return imag;
    }

    public ComplexNumber(double real, double imag){
        this.real = real;
        this.imag = imag;
    }

    public double norm(){
        return Math.sqrt(getReal()* getReal() + getImag()* getImag());
    }

    public double phase(){
        return Math.atan2(getReal(), getImag());
    }

    public ComplexNumber pow(double n){
        double r = norm();
        double theta = Math.atan2(getReal(), getImag());
        r = Math.pow(r, n);
        theta *= n;

        return ComplexNumber.fromPolar(r, theta);
    }

    public ComplexNumber add(ComplexNumber cn){
        return new ComplexNumber(this.getReal() + cn.getReal(), this.getImag() + cn.getImag());
    }

    public ComplexNumber add(double val){
        return new ComplexNumber(this.getReal() + val, this.getImag());
    }

    public ComplexNumber subtract(ComplexNumber cn){
        return new ComplexNumber(this.getReal() - cn.getReal(), this.getImag() - cn.getImag());
    }

    public ComplexNumber subtract(double val){
        return add(-val);
    }

    public ComplexNumber mult(ComplexNumber cn){
        return new ComplexNumber(this.getReal()*cn.getReal() - this.getImag()*cn.getImag(), this.getReal()*cn.getImag() + this.getImag()*cn.getReal());
    }

    public ComplexNumber mult(double d){
        return new ComplexNumber(d*this.getReal(), d*this.getImag());
    }

    public ComplexNumber div(ComplexNumber cn){
        return ComplexNumber.fromPolar(this.norm()/cn.norm(), this.phase() - cn.phase());
    }

    public ComplexNumber div(double d){
        return mult(1/d);
    }

    public ComplexNumber conj(){
        return new ComplexNumber(getReal(), -getImag());
    }

    public static ComplexNumber fromPolar(double r, double theta){
        return new ComplexNumber(r*Math.cos(theta), r*Math.sin(theta));
    }
}
