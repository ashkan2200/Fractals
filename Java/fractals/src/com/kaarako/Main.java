package com.kaarako;

import javax.swing.*;

public class Main {

    public static void main(String[] args) {
        // write your code here
        FractalDrawer p = new FractalDrawer();
        p.registerFractal(new Mandlbrot());
        p.registerFractal(new JuliaSet());
        p.registerFractal(new Dragon());

        p.addKeyListener(p);
        JFrame app = new JFrame();
        app.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        app.add(p);
        app.setSize(600, 500);
        app.setVisible(true);

        app.addKeyListener(p);

    }
}
