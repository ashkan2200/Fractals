#ifndef JULIASET_H
#define JULIASET_H

#include "fractals.h"
#include "states.h"

class juliaset : public fractals
{
public:
    juliaset();

    void paint(QPainter*, const int, const int);
    void setState(states *);
    states * getState();
private:
    states * _state;
};

#endif // JULIASET_H
