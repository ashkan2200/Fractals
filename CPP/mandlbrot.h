#ifndef MANDLBROT_H
#define MANDLBROT_H

#include "fractals.h"
#include "states.h"

class mandlbrot : public fractals
{
public:
    mandlbrot();

    void paint(QPainter*, const int, const int);
    void setState(states *);
    states * getState();

private:
    states * _state;
};

#endif // MANDLBROT_H
