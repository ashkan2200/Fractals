#ifndef FRACTALS_H
#define FRACTALS_H

#include <QPainter>
#include <states.h>


class fractals
{
public:
    virtual void paint(QPainter*, const int, const int);
    virtual void setState(states *);
    virtual states * getState();
};

#endif // FRACTALS_H
