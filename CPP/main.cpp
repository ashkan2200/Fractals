#include "dialog.h"
#include <QApplication>
#include "juliaset.h"
#include "mandlbrot.h"
#include "dragon.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Dialog w;
    w.registerFractal(new mandlbrot);
    w.registerFractal(new juliaset);
    w.registerFractal(new dragon());
    w.show();

    return a.exec();
}
