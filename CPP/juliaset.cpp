#include "juliaset.h"
#include <limits.h>
#include <cmath>
#include <iostream>
#include <thread>


// Worker for each thread
void worker(QColor * colors,
          const int startW,
          const int endW,
          const int startH,
          const int endH,
          const int w,
          const int h,
          const double zoom,
          const double moveX,
          const double moveY,
          const int maxIterations);

juliaset::juliaset(){
    _state = states::newInstance();
}

void juliaset::paint(QPainter* painter, const int w, const int h){
    QPen pen;

    // Get the number of threads
    int NUM_THREADS = _state->getNumThreads();
    // Create a color map
    QColor* colors = new QColor[w*h];
    // Create threads
    std::thread* threads = new std::thread[NUM_THREADS];
    // Compute the chunk of data each thread has to process
    int chunkW = w/NUM_THREADS;

    // Start the threads
    for(int i = 0; i < NUM_THREADS - 1; i++)
        threads[i] = std::thread(&worker, colors,
                                 i*chunkW, (i+1)*chunkW,
                                 0, h,
                                 w, h,
                                 _state->getZoom(), _state->getMoveX(),
                                 _state->getMoveY(),  _state->getIteration());

    // Set the end thread to do the rest of the work
    threads[NUM_THREADS-1] = std::thread(&worker, colors,
                                         (NUM_THREADS-1)*chunkW, w,
                                         0, h,
                                         w, h,
                                         _state->getZoom(), _state->getMoveX(),
                                         _state->getMoveY(),  _state->getIteration());

    // Join threads
    for(int i = 0; i < NUM_THREADS; i++)
        threads[i].join();


    // Use color map to fill the painter
    for(int x = 0; x < w; x++){
        for(int y = 0; y < h; y++){
            pen.setColor(colors[x + y*w]);
            painter->setPen(pen);
            painter->drawPoint(QPointF(x,y));
        }
    }

    // Delete the allocated memory
    delete[] threads;
    delete[] colors;
}

void worker(QColor * colors,
          const int startW,
          const int endW,
          const int startH,
          const int endH,
          const int w,
          const int h,
          const double zoom,
          const double moveX,
          const double moveY,
          const int maxIterations){

    int i;
    double cRe, cIm;                   //real and imaginary part of the constant c, determinate shape of the Julia Set
    double newRe, newIm, oldRe, oldIm;   //real and imaginary parts of new and old

    //pick some values for the constant c, this determines the shape of the Julia Set
    cRe = -0.7;
    cIm = 0.27015;
    for(int x = startW; x < endW; x++){
        for(int y = startH; y < endH; y++){
            newRe = 1.5 * (x - w / 2) / (0.5 * zoom * w) + moveX;
            newIm = (y - h / 2) / (0.5 * zoom * h) + moveY;
            //start the iteration process
            for(i = 0; i < maxIterations; i++){
                //remember value of previous iteration
                oldRe = newRe;
                oldIm = newIm;
                //the actual iteration, the real and imaginary part are calculated
                newRe = oldRe * oldRe - oldIm * oldIm + cRe;
                newIm = 2 * oldRe * oldIm + cIm;
                //if the point is outside the circle with radius 2: stop
                if((newRe * newRe + newIm * newIm) > 4) break;
            }
            // use color model conversion to get rainbow palette, make brightness black if maxIterations reached
            colors[x + y*w] = QColor::fromHsv(i % 256, 255, 255 * (i < maxIterations));
        }
    }
}

void juliaset::setState(states *s){
    _state = s;
}

states *juliaset::getState(){
    return _state;
}
