#include "dragon.h"

using std::vector;

dragon::dragon(){
    _state = states::newInstance();
    _state->setItr(10);
    _state->setRate(1);
    _state->setIterationChangeType(OP::ADD);
}

void dragon::paint(QPainter * painter, const int w, const int h){
    vector<char> directions;
    dragon_birth(_state->getIteration(), directions);

    vector<int> counts;
    divergance(directions, counts);

    double lineL = 10;

    lineL = w/(counts[0] + counts[2]);
    if(lineL > h/(counts[1] + counts[3]))
        lineL = h/(counts[1] + counts[3]);
    if(lineL < 1)
        lineL = 1;

    QColor color = Qt::red;
    QPen pen;
    pen.setColor(color);
    painter->setPen(pen);
    {
        QPixmap pixmap( w, h);
        pixmap.fill( Qt::black );
        painter->drawPixmap(0,0,w,h,pixmap);
    }
    QPoint startP, endP;
    endP.setX( lineL*counts[2]+10);
    endP.setY( lineL*counts[1]+10);
    for(char d : directions){
        startP = endP;
        switch (d) {
        case 0:
            endP.setX(startP.x() + lineL);
            break;
        case 1:
            endP.setY(startP.y() - lineL);
            break;
        case 2:
            endP.setX(startP.x() - lineL);
            break;
        case 3:
            endP.setY(startP.y() + lineL);
            break;
        }
        painter->drawLine(startP, endP);
    }
}

// Direction : 0 = east, 1 = north, 2 = west, 3 = south.
void dragon::dragon_birth(int iteration, vector<char> &directions){
    directions.push_back(0);
    vector<char> temp;
    for( int i = 0; i < iteration; i++){
        for( vector<char>::reverse_iterator x = directions.rbegin(); x != directions.rend(); x++ )
            temp.push_back( ((*x)+1)%4 );
        temp.insert(temp.end(), directions.begin(), directions.end());

        directions = temp;
        temp.clear();
    }
}

void dragon::divergance(vector<char>& directions, vector<int> & counts){
    int hor_div = 0;
    int ver_div = 0;
    counts.push_back(0);
    counts.push_back(1);
    counts.push_back(2);
    counts.push_back(3);
    for(char s: directions){
        switch (s) {
        case 0:
            hor_div++;
            break;
        case 1:
            ver_div--;
            break;
        case 2:
            hor_div--;
            break;
        case 3:
            ver_div++;
            break;
        default:
            break;
        }
        if(hor_div >= 0 && counts[0] < hor_div)
            counts[0] = hor_div;
        if(hor_div <= 0 && counts[2] < -hor_div)
            counts[2] = -hor_div;
        if(ver_div <= 0 && counts[1] < -ver_div)
            counts[1] = -ver_div;
        if(ver_div >= 0 && counts[3] < ver_div)
            counts[3] = ver_div;

    }
}

void dragon::setState(states *s){
    _state = s;
}

states* dragon::getState(){
    return _state;
}
