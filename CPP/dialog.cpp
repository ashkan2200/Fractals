#include "dialog.h"
#include "ui_dialog.h"
#include <map>
using std::map;
using std::string;
static map<string, string> help_map = {
    {"+" , "Add iterations"},
    {"-" , "Reduce iterations"},
    {"r" , "Reset view"},
    {"f" , "Next fractal"},
    {"arrow keys" , "Move around"},
    {"a" , "More threads"},
    {"s" , "Less threads"},
    {"z" , "Zoom in"},
    {"x" , "Zoom out"},
    {"q" , "Quit application"},
    {"h" , "Toggle help"}
};

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    fractal_top = 0;
    bakupFractal = new fractals;
    show_help = true;
}

void Dialog::registerFractal(fractals * fr){
    frctls.push_back(fr);
}

Dialog::~Dialog(){
    delete ui;
}

void Dialog::draw_help(QPainter& painter){
    int font_size = 9;
    int hor_p = 0;
    int ver_p = font_size+1;
    int ver_spacing = font_size+1;
    QPen pen;
    QFont font = painter.font() ;
    font.setPixelSize(font_size);
    painter.setFont(font);
    QFontMetrics qfm(font);


    int temp = 0;
    for(auto it = help_map.begin(); it != help_map.end(); it++) {
        temp = qfm.width(QString::fromStdString(it->first)) + qfm.width(QString::fromStdString(it->second)) + 3;
        if (temp > hor_p)
            hor_p = temp;
    }

    pen.setColor(Qt::white);
    painter.setPen(pen);
    QString qst("Holding <shift> performs the actions faster");
    painter.drawText(QPointF(width() - qfm.width(qst) - 10, ver_p), qst);
    ver_p += ver_spacing;
    for(auto it = help_map.begin(); it != help_map.end(); it++) {
        qst = QString::fromStdString(it->first);
        qst += " : ";
        qst += QString::fromStdString(it->second);
        painter.drawText(QPointF(width() - hor_p - 10, ver_p), qst);
        ver_p += ver_spacing;
    }
    qst = "Number of threads: " + QString::number(frctls[fractal_top]->getState()->getNumThreads());
    painter.drawText(QPointF(width() - qfm.width(qst) - 10, ver_p), qst);
    ver_p += ver_spacing;
    qst = "Number of Iterations: " + QString::number(frctls[fractal_top]->getState()->getIteration());
    painter.drawText(QPointF(width() - qfm.width(qst) - 10, ver_p), qst);
}

void Dialog:: paintEvent(QPaintEvent *)
{
    QPixmap pix(width(),height());
    QPainter pixpaint(&pix);
    if (frctls.size() > 0)
        frctls[fractal_top]->paint(&pixpaint, width(), height());
    else
        bakupFractal->paint(&pixpaint, width(), height());

    pixpaint.end();

    QPainter painter;
    painter.begin(this);
    painter.setRenderHint(QPainter::Antialiasing, true);
    painter.drawPixmap(0,0,width(),height(),pix);
    if(show_help)
        draw_help(painter);
    painter.end();
}

void Dialog::keyPressEvent(QKeyEvent *k)
{
    int shiftval = (k->modifiers() & Qt::ShiftModifier) ? 1 : 0;
    int nt;
    fractals *fs = frctls[fractal_top];
    switch (k->key()) {
    case Qt::Key_F:
        fractal_top = (fractal_top + 1)%frctls.size();
        break;
    case Qt::Key_Plus:
    case Qt::Key_Equal:
        fs->getState()->addItr();
        break;
    case Qt::Key_Minus:
        fs->getState()->reduceItr();
        break;
    case Qt::Key_R:
        fs->getState()->reset();
        break;
    case Qt::Key_Right:
        fs->getState()->moveRight(shiftval*3. + 1.);
        break;
    case Qt::Key_Left:
        fs->getState()->moveLeft(shiftval*3. + 1.);
        break;
    case Qt::Key_Up:
        fs->getState()->moveUp(shiftval*3. + 1.);
        break;
    case Qt::Key_Down:
        fs->getState()->moveDown(shiftval*3. + 1.);
        break;
    case Qt::Key_Z:
        fs->getState()->zoomIn(shiftval*3. + 1.);
        break;
    case Qt::Key_X:
        fs->getState()->zoomOut(shiftval*3. + 1.);
        break;
    case Qt::Key_H:
        show_help = !show_help;
    case Qt::Key_A:
        nt = fs->getState()->getNumThreads();
        nt +=(shiftval*3. + 1.);
        fs->getState()->setNumThreads(nt);
        break;
    case Qt::Key_S:
        nt = fs->getState()->getNumThreads();
        if(nt > (shiftval*3 + 1)){
            nt -=(shiftval*3. + 1.);
            fs->getState()->setNumThreads(nt);
        }else
            fs->getState()->setNumThreads(1);
        break;
    case Qt::Key_Q:                               // quit
        QApplication::exit();
        break;
    }
    update();
}
