#-------------------------------------------------
#
# Project created by QtCreator 2016-01-21T23:30:48
#
#-------------------------------------------------

QT       += core gui

#QMAKE_CC = gcc-5
#QMAKE_CXX = g++-5

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Fractals
TEMPLATE = app


SOURCES += main.cpp\
        dialog.cpp \
    fractals.cpp \
    mandlbrot.cpp \
    juliaset.cpp \
    states.cpp \
    dragon.cpp

HEADERS  += dialog.h \
    fractals.h \
    mandlbrot.h \
    juliaset.h \
    states.h \
    dragon.h

FORMS    += dialog.ui

CONFIG += c++11
