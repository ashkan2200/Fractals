#include <cmath>
#include "states.h"

states::states(){
    itType = OP::MULT;
    _rate = 2;
    _maxIterations = 300;
    reset();
}

states *states::newInstance(){
    return new states;
}

void states::reset(){
    moveUnitValue = 0.001;
    zoomUnitValue = 1.001;
    frameTime = 64;
    zoom = 1;
    moveX = 0;
    moveY = 0;
    maxIterations = _maxIterations;
    num_threads = 1;
}

void states::zoomIn(const double ratio){
    zoom *= ratio*pow(zoomUnitValue, frameTime);
}

void states::zoomOut(const double ratio){
    zoom /= ratio*pow(zoomUnitValue, frameTime);
}

void states::moveLeft(const double ratio){
    moveX -= ratio* moveUnitValue * frameTime / zoom;
}

void states::moveRight(const double ratio){
    moveX += ratio * moveUnitValue * frameTime / zoom;
}


void states::moveUp(const double ratio){
    moveY -= ratio* moveUnitValue * frameTime / zoom;
}

void states::moveDown(const double ratio){
    moveY += ratio* moveUnitValue * frameTime / zoom;
}

void states::setItr(int it){
    maxIterations = it;
    _maxIterations = it;
}

void states::setRate(int r){
    _rate = r;
}

void states::addItr(){
    switch (itType) {
    case OP::ADD:
        maxIterations += _rate;
        break;
    case OP::MULT:
        maxIterations *= _rate;
        break;
    }
}

void states::reduceItr(){
    if (maxIterations > _rate){
        switch (itType) {
        case OP::ADD:
            maxIterations -= _rate;
            break;
        case OP::MULT:
            maxIterations /= _rate;
            break;
        }
    }
}

double states::getZoom(){
    return zoom;
}

double states::getMoveX(){
    return moveX;
}

double states::getMoveY(){
    return moveY;
}

int states::getIteration(){
    return maxIterations;
}

void states::setIterationChangeType(OP op){
    itType = op;
}

int states::getNumThreads()
{
    return num_threads;
}

void states::setNumThreads(const int nt)
{
    num_threads = nt;
}
