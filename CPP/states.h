#ifndef STATES_H
#define STATES_H

enum class OP{
    ADD,
    MULT
};

class states
{
public:
    static states* newInstance();
    void reset();
    void zoomIn(const double ratio = 1);
    void zoomOut(const double ratio = 1);
    void moveUp(const double ratio = 1);
    void moveDown(const double ratio = 1);
    void moveLeft(const double ratio = 1);
    void moveRight(const double ratio = 1);
    void addItr();
    void reduceItr();
    void setItr(int it);
    void setRate(int r);
    double getZoom();
    double getMoveX();
    double getMoveY();
    int getIteration();
    void setIterationChangeType(OP op);
    int getNumThreads();
    void setNumThreads(const int nt);
private:
    states();
    OP itType;

    double moveUnitValue;
    double zoomUnitValue;
    double frameTime;
    double zoom;
    double moveX;
    double moveY;
    int maxIterations;
    int _maxIterations;
    int _rate;
    int num_threads;
};

#endif // STATES_H
