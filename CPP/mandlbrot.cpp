#include <thread>
#include "mandlbrot.h"

// Worker for each thread
void work(QColor * colors,
          const int startW,
          const int endW,
          const int startH,
          const int endH,
          const int w,
          const int h,
          const double zoom,
          const double moveX,
          const double moveY,
          const int maxIterations);

mandlbrot::mandlbrot(){
    _state = states::newInstance();
}

void mandlbrot::paint(QPainter* painter, const int w, const int h){
    QPen pen;

    // Get the number of threads
    int NUM_THREADS = _state->getNumThreads();
    // Create a color map
    QColor* colors = new QColor[w*h];
    // Create threads
    std::thread* threads = new std::thread[NUM_THREADS];
    // Compute the chunk of data each thread has to process
    int chunkW = w/NUM_THREADS;

    // Start the threads
    for(int i = 0; i < NUM_THREADS - 1; i++)
        threads[i] = std::thread(&work, colors,
                                 i*chunkW, (i+1)*chunkW,
                                 0, h,
                                 w, h,
                                 _state->getZoom(), _state->getMoveX(),
                                 _state->getMoveY(),  _state->getIteration());

    // Set the end thread to do the rest of the work
    threads[NUM_THREADS-1] = std::thread(&work, colors,
                                         (NUM_THREADS-1)*chunkW, w,
                                         0, h,
                                         w, h,
                                         _state->getZoom(), _state->getMoveX(),
                                         _state->getMoveY(),  _state->getIteration());

    // Join threads
    for(int i = 0; i < NUM_THREADS; i++)
        threads[i].join();


    // Use color map to fill the painter
    for(int x = 0; x < w; x++){
        for(int y = 0; y < h; y++){
            pen.setColor(colors[x + y*w]);
            painter->setPen(pen);
            painter->drawPoint(QPointF(x,y));
        }
    }

    // Delete the allocated memory
    delete[] threads;
    delete[] colors;
}

void work(QColor * colors,
          const int startW,
          const int endW,
          const int startH,
          const int endH,
          const int w,
          const int h,
          const double zoom,
          const double moveX,
          const double moveY,
          const int maxIterations){

    int i;
    double pr, pi;                       // real and imaginary part of the pixel p
    double newRe, newIm, oldRe, oldIm;   // real and imaginary parts of new and old z

    for(int x = startW; x < endW; x++){
        for(int y = startH; y < endH; y++){
            pr = 1.5 * (x - w / 2) / (0.5 * zoom * w) + moveX;
            pi = (y - h / 2) / (0.5 * zoom * h) + moveY;
            newRe = newIm = oldRe = oldIm = 0; // these should start at 0,0
            // start the iteration process
            for(i = 0; i < maxIterations; i++){
                // remember value of previous iteration
                oldRe = newRe;
                oldIm = newIm;
                // the actual iteration, the real and imaginary part are calculated
                newRe = oldRe * oldRe - oldIm * oldIm + pr;
                newIm = 2 * oldRe * oldIm + pi;
                // if the point is outside the circle with radius 2: stop
                if((newRe * newRe + newIm * newIm) > 4) break;
            }
            // use color model conversion to get rainbow palette, make brightness black if maxIterations reached
            colors[x + y*w] = QColor::fromHsv(i % 256, 255, 255 * (i < maxIterations));
        }
    }
}

void mandlbrot::setState(states *s){
    _state = s;
}

states *mandlbrot::getState(){
    return _state;
}
