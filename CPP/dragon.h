#ifndef DRAGON_H
#define DRAGON_H

#include "fractals.h"
#include "states.h"
#include <vector>

class dragon : public fractals
{
public:
    dragon();

    void paint(QPainter*, const int, const int);
    void setState(states *);
    states * getState();
private:
    states * _state;
    void dragon_birth(int iteration, std::vector<char> &directions);
    void divergance(std::vector<char>& directions, std::vector<int> &counts);
};

#endif // DRAGON_H
