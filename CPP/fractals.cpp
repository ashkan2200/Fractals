#include "fractals.h"


void fractals::paint(QPainter* painter, const int w, const int h){
    QColor color; //the RGB color value for the pixel
    QPen pen;
    {
        QPixmap pixmap( w, h);
        pixmap.fill( Qt::white );
        painter->drawPixmap(0,0,w,h,pixmap);
    }
    color = Qt::black;
    pen.setColor(color);
    painter->setPen(pen);
    painter->drawText(QPointF(100,h/2), "No fractal is implemented yet!!!");
}

void fractals::setState(states *){
    return;
}

states *fractals::getState(){
    return states::newInstance();
}
