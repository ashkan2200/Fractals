#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QPainter>
#include <QKeyEvent>
#include <vector>
#include <functional>
#include "fractals.h"

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    void registerFractal(fractals *);
    ~Dialog();

private:
    void draw_help(QPainter &);
    Ui::Dialog *ui;
    std::vector<fractals *> frctls;
    fractals *bakupFractal;
    int fractal_top;
    bool show_help;
protected:
    void paintEvent(QPaintEvent *e);
    void keyPressEvent( QKeyEvent *k );
};

#endif // DIALOG_H
